#!/bin/bash

# get current kernel and realtime patch version
VERSION=5.15
FILE=`curl -s https://mirrors.edge.kernel.org/pub/linux/kernel/projects/rt/${VERSION}/`
KERNEL=70
RTPATCH=50
echo "using kernel $VERSION.$KERNEL with realtime patch $RTPATCH"

# get kernel file names and location

KERNELURL=https://www.kernel.org/pub/linux/kernel/v5.x/
KERNELNAME=linux-${VERSION}.${KERNEL}
KERNELFILE=linux-${VERSION}.${KERNEL}.tar
KERNELFILEXZ=linux-${VERSION}.${KERNEL}.tar.xz
KERNELSIGN=linux-${VERSION}.${KERNEL}.tar.sign
PATCHURL=https://www.kernel.org/pub/linux/kernel/projects/rt/${VERSION}/
PATCHFILE=patch-${VERSION}.${KERNEL}-rt${RTPATCH}.patch
PATCHFILEXZ=patch-${VERSION}.${KERNEL}-rt${RTPATCH}.patch.xz
PATCHSIGN=patch-${VERSION}.${KERNEL}-rt${RTPATCH}.patch.sign

# download kernel

echo "downloading kernel from ${KERNELURL}${KERNELFILEXZ}"
curl -SLO ${KERNELURL}${KERNELFILEXZ}
echo "downloading kernel signature from ${KERNELURL}${KERNELSIGN}"
curl -SLO ${KERNELURL}${KERNELSIGN}
echo "downloading patch file from ${PATCHURL}${PATCHFILEXZ}"
curl -sLO ${PATCHURL}${PATCHFILEXZ}
echo "downloading patch signature from ${PATCHURL}${PATCHSIGN}"
curl -sLO ${PATCHURL}${PATCHSIGN}

# unzip kernel (required for verification)

xz -d $KERNELFILEXZ
xz -d $PATCHFILEXZ

# verify the kernel

gpg --verify --keyserver hkp://keys.gnupg.net --auto-key-retrieve $KERNELSIGN
gpg --verify --keyserver hkp://keys.gnupg.net --auto-key-retrieve $PATCHSIGN

# unpack kernel

tar xf $KERNELFILE
cd $KERNELNAME
patch -p1 < ../${PATCHFILE}

# build the kernel

# use the kernel config build on a normal ubuntu 20.4
cp ../config .config
# update config with defaults if new options have been added
make olddefconfig
make deb-pkg
